import { Component, OnInit } from '@angular/core';
import { IContact } from 'src/app/interfaces/contact.interface';
import { ContactService } from 'src/app/services/contact.service';

@Component({
  selector: 'app-contact-view',
  templateUrl: './contact-view.component.html',
  styles: [],
})
export class ContactViewComponent implements OnInit {
  contacts: IContact[] = [];

  constructor(private contactService: ContactService) {}

  ngOnInit(): void {
    this.contactService.getContacts().subscribe((res) => {
      this.contacts = res.data;
    });
  }

  onContactDelete(id: string | number) {
    const toDelete: boolean = confirm(
      'Delete kardu kya bhaila? Pacche na kiye'
    );
    if (toDelete) {
      this.contactService.deleteContact(id.toString()).subscribe((res: any) => {
        if (res.data) {
          window.location.href = '/';
        }
      });
    }
  }
}
