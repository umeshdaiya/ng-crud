import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ContactService } from 'src/app/services/contact.service';

@Component({
  selector: 'app-contact-insert',
  templateUrl: './contact-insert.component.html',
  styles: [],
})
export class ContactInsertComponent implements OnInit {
  contactForm: FormGroup = new FormGroup({
    name: new FormControl(''),
    contact: new FormControl(''),
  });

  constructor(private contactService: ContactService) {}

  ngOnInit(): void {}

  onInsertFormSubmit() {
    const { name, contact } = this.contactForm.value;

    this.contactService
      .insertContact({ name: name, contact })
      .subscribe((res: any) => {
        if (res.data) {
          alert('Inserted Successfully!!!!~~~');
        }

        if (res.error) {
          alert('Pato koni bhaila kai to galti huygi');
        }

        this.contactForm.reset();
      });
  }
}
