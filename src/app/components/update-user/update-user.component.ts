import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { IContact } from 'src/app/interfaces/contact.interface';
import { ContactService } from 'src/app/services/contact.service';

@Component({
  selector: 'app-update-user',
  templateUrl: './update-user.component.html',
  styles: [],
})
export class UpdateUserComponent implements OnInit {
  contactForm: FormGroup = new FormGroup({
    name: new FormControl(''),
    contact: new FormControl(''),
  });

  contacts: IContact[] = [];

  constructor(
    private contactService: ContactService,
    private route: ActivatedRoute,
    private router: Router,
    private fb: FormBuilder
  ) {}

  private contId: string | undefined;

  ngOnInit(): void {
    this.route.paramMap.subscribe((res: any) => (this.contId = res.params.id));

    //contId ke baad ! lagaya hai kyu ki hame pata hai undefined nahi hoga string hi milegi
    this.contactService.getOneContact(this.contId!).subscribe((res: any) => {
      this.contacts = res.data;

      this.contactForm = this.fb.group({
        name: res.data.attributes.name,
        contact: res.data.attributes.contact,
      });
    });
  }

  onUpdateFormSubmit() {
    this.contactService
      .updateContact({
        id: this.contId,
        name: this.contactForm.value.name,
        contact: this.contactForm.value.contact,
      })
      .subscribe((res: any) => {
        this.contactForm.reset();
        this.router.navigate(['/']);
      });
  }
}
