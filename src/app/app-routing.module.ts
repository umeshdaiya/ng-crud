import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ContactInsertComponent } from './components/contact-insert/contact-insert.component';
import { ContactViewComponent } from './components/contact-view/contact-view.component';
import { UpdateUserComponent } from './components/update-user/update-user.component';

const routes: Routes = [
  { path: '', component: ContactViewComponent },
  { path: 'edit/:id', component: UpdateUserComponent },
  { path: 'insert', component: ContactInsertComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
