import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { IContact, IContactInsert } from '../interfaces/contact.interface';

@Injectable({
  providedIn: 'root',
})
export class ContactService {
  constructor(private http: HttpClient) {}

  private baseUrl: string = 'http://localhost:1337/api';

  getContacts(): Observable<any> {
    return this.http.get<any>(`${this.baseUrl}/dirs`);
  }

  insertContact(body: IContactInsert) {
    const { name, contact } = body;
    return this.http.post(`${this.baseUrl}/dirs`, {
      data: { name: name, contact: contact },
    });
  }

  getOneContact(id: string) {
    return this.http.get(`${this.baseUrl}/dirs/${id}`);
  }

  updateContact(body: IContactInsert) {
    const { id, name, contact } = body;

    return this.http.put(`${this.baseUrl}/dirs/${id}`, {
      data: { name: name, contact: contact },
    });
  }

  deleteContact(id: string) {
    return this.http.delete(`${this.baseUrl}/dirs/${id}`);
  }
}
