export interface IContact {
  id: number | string;
  attributes: {
    name: string;
    contact: string;
  };
}

export interface IContactInsert {
  id?: string;
  name: string;
  contact: string;
}
