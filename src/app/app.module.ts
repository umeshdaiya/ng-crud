import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { UpdateUserComponent } from './components/update-user/update-user.component';

import { HttpClientModule } from '@angular/common/http';
import { ContactInsertComponent } from './components/contact-insert/contact-insert.component';
import { ContactViewComponent } from './components/contact-view/contact-view.component';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    UpdateUserComponent,
    ContactInsertComponent,
    ContactViewComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    ReactiveFormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
